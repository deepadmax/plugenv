import venv
import subprocess
import os
import requests

from pathlib import Path
from hashlib import sha256

from werkzeug.http import parse_options_header



class PlugEnv:
    def __init__(self, path: str):
        self.path = Path(path).absolute()

        # Create virtual environment
        venv.create(
            self.path,
            with_pip=True,
            system_site_packages=True
        )

        # Cache for installing plugins
        self.install_cache_path = path / 'cache'
        os.makedirs(self.install_cache_path, exist_ok=True)


    @property
    def python_path(self):
        """Path to Python inside environment"""
        return self.path / 'bin' / 'python'


    # ENVIRONMENT TERMINAL COMMANDS

    def python(self, *args):
        """Run Python command"""
        return subprocess.run([self.python_path, *args], capture_output=True, text=True)

    def pip(self, *args):
        """Run PIP command"""
        self.python('-m', 'pip', *args)

    def upgrade_pip(self):
        """Upgrade PIP if update is available"""
        self.pip('install', '--upgrade', 'pip')

    def install_package(self, package: str):
        """Install package inside environment"""
        installed = {name for name, _ in self.installed_packages()}

        if package not in installed:
            self.pip('install', package)

    
    def installed_packages(self):
        """All installed packages"""

        # Get installed packages from `pip list`
        pkgs_string = self.python('-m', 'pip', 'list').stdout

        for line in pkgs_string.splitlines():
            name, version = line.split(maxsplit=1)
            yield name, version


    # PLUGIN MANAGEMENT

    def install_plugin(self, url: str):
        """Install package as plugin"""
        
        file = requests.get(url)


        fname = parse_options_header(file.headers['Content-Disposition'])[1]['filename']

        # Generate unique package name from URL
        m = sha256()
        m.update(str.encode(url, encoding='UTF-8'))
        url_hash = m.hexdigest()

        plugin_name = f'__plugins__{url_hash}'

        print(plugin_name)